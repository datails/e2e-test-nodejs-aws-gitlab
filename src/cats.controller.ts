import {
  Body,
  Controller,
  Get,
  Post,
  Head,
  Param,
  Header,
  ValidationPipe,
} from "@nestjs/common";
import { ApiResponse, ApiTags } from "@nestjs/swagger";
import { CatsService } from "./cats.service";
import { Cat } from "./cats.dto";

@ApiTags("Cats")
@Controller()
export class CatsController {
  constructor(private readonly catsService: CatsService) {}

  @ApiResponse({ status: 200, description: "Create a cat.", type: [Cat] })
  @Header("Content-Type", "application/json")
  @Post()
  async create(@Body(new ValidationPipe({ transform: true })) cat: Cat) {
    return this.catsService.create(cat);
  }

  @ApiResponse({ status: 200, description: "Get one cat.", type: Cat })
  @Header("Content-Type", "application/json")
  @Get(":name")
  async findOne(@Param("name") name: string) {
    return this.catsService.findOne(name);
  }

  @Header("Content-Type", "application/json")
  @Head("/health")
  async health() {
    return 'healthy app!'
  }
}
