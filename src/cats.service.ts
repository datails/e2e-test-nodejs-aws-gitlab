import { Injectable } from "@nestjs/common";
import { InjectModel, Model } from "nestjs-dynamoose";
import { Cat } from "./cats.dto";

@Injectable()
export class CatsService {
  constructor(
    @InjectModel(process.env.AWS_TABLE_NAME)
    private readonly catsModel: Model<Cat, string>
  ) {}

  async create(cat: Cat) {
    if (await this.findOne(cat.name)) {
      return this.catsModel.update(cat);
    }

    return this.catsModel.create(cat);
  }

  findOne(name: string) {
    return this.catsModel.get(name);
  }
}
