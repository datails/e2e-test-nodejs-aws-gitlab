import { Module } from "@nestjs/common";
import { DynamooseModule } from "nestjs-dynamoose";

import { CatsController } from "./cats.controller";
import { CatsService } from "./cats.service";
import { CatsSchema } from "./cats.schema";

@Module({
  imports: [
    DynamooseModule.forRoot({
      aws: {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        region: process.env.AWS_REGION,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
      },
      local: process.env.AWS_DYNAMO_ENDPOINT ?? false,
    }),
    DynamooseModule.forFeature([
      {
        name: process.env.AWS_TABLE_NAME,
        schema: CatsSchema,
      },
    ]),
  ],
  controllers: [CatsController],
  providers: [CatsService],
})
export class CatsModule {}
