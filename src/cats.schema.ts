import { Schema } from "dynamoose";

export const CatsSchema: any = new Schema(
  {
    name: {
      type: String,
      required: true,
      hashKey: true,
    },
    age: {
      type: Number,
      required: true,
    },
    breed: {
      type: String,
      required: true,
    },
  },
  {
    saveUnknown: true,
  }
);
