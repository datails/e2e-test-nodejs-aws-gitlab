import request from "supertest";

describe("cats (e2e)", () => {
  const req = request("http://localhost:3000");
  const cat = {
    name: "meow",
    age: 1,
    breed: "foo",
  };

  test("create a cat", async () => {
    await req
      .post("/cats")
      .set("Content-Type", "application/json")
      .send(cat)
      .expect((res) => {
        expect(res.body).toMatchObject(cat);
      })
      .expect(201);
  });

  test("get a cat", async () => {
    await req
      .get("/cats/meow")
      .expect((res) => {
        expect(res.body).toMatchObject(cat);
      })
      .expect(200);
  });
});
