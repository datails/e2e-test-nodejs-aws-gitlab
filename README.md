# Setup E2E tests with GitLab CI
A boilerplate with a NodeJS app that depends on AWS services and Redis.


## Working
Exposes 1 api both on `http://localhost:3000`.

## Start the app

```bash
docker-compose up
```

### Create a cat
```bash
curl --location --request POST 'http://localhost:3000/cats' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "meow",
    "age": 1,
    "breed": "cat"
}'
```

### Get a cat
```bash
curl http://localhost:3000/cats/meow
```